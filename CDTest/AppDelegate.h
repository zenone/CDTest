//
//  AppDelegate.h
//  CDTest
//
//  Created by izhikang on 2017/11/4.
//  Copyright © 2017年 izhikang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

